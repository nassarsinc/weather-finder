package tournament.league.live.cricket.com.mvp.network;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import tournament.league.live.cricket.com.mvp.utility.Constant;

public class ApiClient{

    private static OkHttpClient okHttpClient = null;
    private static Request request = null;

    public static synchronized OkHttpClient getInstance() {
        if(okHttpClient == null)
            okHttpClient = new OkHttpClient();
        return okHttpClient;
    }

    public static synchronized Request getClient(String lat, String lng) {
        if(request == null)
            request = new Request.Builder()
                    .url(Constant.BASE_URL + "/" + lat + "," + lng)
                    .build();
        return request;
    }
}
