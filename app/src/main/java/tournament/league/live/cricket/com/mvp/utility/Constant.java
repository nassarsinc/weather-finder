package tournament.league.live.cricket.com.mvp.utility;

import android.Manifest;

public class Constant {

//    Weather end point
    public static final String BASE_URL = "https://api.darksky.net/forecast/2bb07c3bece89caf533ac9a5d23d8417";
//    Location Permission
    public static final int FINE_LOCATION_PERMISSION_CODE = 111;
//    Location Permission
    public static final String FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
//    Intetn Action
    public static final String SETTING_ACTION_INTENT = "android.settings.APPLICATION_DETAILS_SETTINGS";
//    Alert Dialog Text
    public static final String ALERT_TITLE = "Application require GPS permission to work";
    public static final String SETTING_BUTTON = "Settings";
//    TAGS
    public static final String SETTING_DIALOG_TAG = "setting_dialog";
//    Text
    public static final String ENABLE_LOCATION_INTERNET = "Enable GPS and Internet to get current weather.";
}
