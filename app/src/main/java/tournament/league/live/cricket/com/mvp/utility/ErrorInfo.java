package tournament.league.live.cricket.com.mvp.utility;

import org.jetbrains.annotations.NotNull;

public class ErrorInfo {

    private int errorCode;
    private String errorStatus;
    private String errorMessage;
    private Object object;

    public ErrorInfo(){

    }

    public ErrorInfo(int errorCode, String errorStatus, String errorMessage, Object object){
        this.errorCode = errorCode;
        this.errorStatus = errorStatus;
        this.errorMessage = errorMessage;
        this.object = object;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Object getObject() {
        return object;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @NotNull
    @Override
    public String toString() {
        return "ErrorInfo [errorCode=" + errorCode + ", errorStatus="
                + errorStatus + ", errorMessage=" + errorMessage + "]";
    }
}
