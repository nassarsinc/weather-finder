package tournament.league.live.cricket.com.mvp.utility;

public enum ErrorUtility {

    NETWORK_ERROR(1, "NETWORK_ERROR", "Sorry, some issue in the network. Try Again!"),
    RESPONSE_ISSUE(2, "RESPONSE_ISSUE", "Some issue with the data. Contact Support"),
    LOCATION_ISSUE(3, "LOCATION_ISSUE", "Some issue finding your location. Try Again!");


    private int code;
    private String name;
    private String message;

    private ErrorUtility(int code, String name, String message) {
        this.code = code;
        this.name = name;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}
