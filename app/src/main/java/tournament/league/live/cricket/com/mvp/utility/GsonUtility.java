package tournament.league.live.cricket.com.mvp.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtility {

    private static Gson gson = null;

    public static synchronized Gson getInstance(){
        if(gson == null)
            gson = new GsonBuilder().create();
        return gson;
    }
}
