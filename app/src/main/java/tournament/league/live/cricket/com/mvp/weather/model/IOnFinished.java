package tournament.league.live.cricket.com.mvp.weather.model;

import tournament.league.live.cricket.com.mvp.utility.ErrorInfo;

public interface IOnFinished {
//    Get Async response
    void onFinished(ErrorInfo errorInfo);
}
