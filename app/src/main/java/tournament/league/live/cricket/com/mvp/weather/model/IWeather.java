package tournament.league.live.cricket.com.mvp.weather.model;

public interface IWeather {
    double getLatitude();
    double getLongitude();
    String getTimezone();
    CurrentlyModel getCurrentlyModel();
    boolean isGpsDisabled();
    boolean isInternetDisabled();
    String getError();
    void setLatitude(double latitude);
    void setLongitude(double longitude);
    void setTimezone(String timezone);
    void setCurrentlyModel(CurrentlyModel currentlyModel);
    void setGpsDisabled(boolean gpsDisabled);
    void setInternetDisabled(boolean internetDisabled);
    void setError(String error);

//    Get weather for user lat and long
    void getWeather(IOnFinished iOnFinished, String lat, String lng);
//    Get Weather Model object from json
    WeatherModel getObject(String json);
}
