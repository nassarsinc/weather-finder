package tournament.league.live.cricket.com.mvp.weather.model;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;
import tournament.league.live.cricket.com.mvp.utility.ErrorInfo;
import tournament.league.live.cricket.com.mvp.utility.ErrorUtility;
import tournament.league.live.cricket.com.mvp.network.ApiClient;
import tournament.league.live.cricket.com.mvp.utility.GsonUtility;

public class WeatherModel extends BaseObservable implements IWeather {

    private double latitude;
    private double longitude;
    private String timezone;
    private CurrentlyModel currently;
    private boolean gpsDisabled;
    private boolean internetDisabled;
    private String error;

    public WeatherModel(double latitude, double longitude, String timezone,
                        CurrentlyModel currently, boolean gpsDisabled,
                        boolean internetDisabled, String error) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timezone = timezone;
        this.currently = currently;
        this.gpsDisabled = gpsDisabled;
        this.internetDisabled = internetDisabled;
        this.error = error;
    }

//    Getters
    @Bindable
    @Override
    public double getLatitude() {
        return latitude;
    }

    @Bindable
    @Override
    public double getLongitude() {
        return longitude;
    }

    @Bindable
    @Override
    public String getTimezone() {
        return timezone;
    }

    @Bindable
    @Override
    public CurrentlyModel getCurrentlyModel() {
        return currently;
    }

    @Bindable
    @Override
    public boolean isGpsDisabled() {
        return gpsDisabled;
    }

    @Bindable
    @Override
    public boolean isInternetDisabled() {
        return internetDisabled;
    }

    @Bindable
    @Override
    public String getError() {
        return error;
    }

//    Setters
    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
        notifyPropertyChanged(BR.latitude);
    }

    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
        notifyPropertyChanged(BR.longitude);
    }

    @Override
    public void setTimezone(String timezone) {
        this.timezone = timezone;
        notifyPropertyChanged(BR.timezone);
    }

    @Override
    public void setCurrentlyModel(CurrentlyModel currently) {
        this.currently = currently;
        notifyPropertyChanged(BR.currentlyModel);
    }

    @Override
    public void setGpsDisabled(boolean gpsDisabled) {
        this.gpsDisabled = gpsDisabled;
        notifyPropertyChanged(BR.gpsDisabled);
    }

    @Override
    public void setInternetDisabled(boolean internetDisabled) {
        this.internetDisabled = internetDisabled;
        notifyPropertyChanged(BR.internetDisabled);
    }

    @Override
    public void setError(String error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    @Override
    public void getWeather(IOnFinished iOnFinished, String lat, String lng){

        ErrorInfo errorInfo = new ErrorInfo();
        ApiClient.getInstance().newCall(ApiClient.getClient(lat, lng)).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                errorInfo.setErrorCode(ErrorUtility.NETWORK_ERROR.getCode());
                errorInfo.setErrorMessage(ErrorUtility.NETWORK_ERROR.getMessage());
                iOnFinished.onFinished(errorInfo);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if(!response.isSuccessful()){
                    errorInfo.setErrorCode(ErrorUtility.RESPONSE_ISSUE.getCode());
                    errorInfo.setErrorMessage(ErrorUtility.RESPONSE_ISSUE.getMessage());
                }else{
                    ResponseBody responseBody = null;
                    if((responseBody = response.body()) != null){
                        errorInfo.setObject(getObject(responseBody.string()));
                    }else{
                        errorInfo.setErrorCode(ErrorUtility.RESPONSE_ISSUE.getCode());
                        errorInfo.setErrorMessage(ErrorUtility.RESPONSE_ISSUE.getMessage());
                    }
                }
                iOnFinished.onFinished(errorInfo);
            }
        });
    }

    @Override
    public WeatherModel getObject(String json){
        return GsonUtility.getInstance().fromJson(json, WeatherModel.class);
    }
}
