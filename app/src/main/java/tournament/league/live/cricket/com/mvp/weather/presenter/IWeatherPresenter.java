package tournament.league.live.cricket.com.mvp.weather.presenter;

public interface IWeatherPresenter {
    void initializeModel();
    void getLocationPermission();
    void getWeather(String lat, String lng);
    void setError(String message);
    void setProgressBar(int visible);
    void closeDialog();
    void openDialog();
    void setGpsDisabled(boolean status);
    void setInternetDisabled(boolean status);
    void isInternetConnected(boolean status);
    void checkInternet();
    void checkGpsInternet();
    void getLocation();
    void getLatLng(double lat, double lng);
}
