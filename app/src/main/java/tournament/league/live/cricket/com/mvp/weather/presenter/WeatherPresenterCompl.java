package tournament.league.live.cricket.com.mvp.weather.presenter;

import tournament.league.live.cricket.com.mvp.utility.Constant;
import tournament.league.live.cricket.com.mvp.utility.ErrorInfo;
import tournament.league.live.cricket.com.mvp.weather.model.IOnFinished;
import tournament.league.live.cricket.com.mvp.weather.model.IWeather;
import tournament.league.live.cricket.com.mvp.weather.model.WeatherModel;
import tournament.league.live.cricket.com.mvp.weather.view.IWeatherView;

public class WeatherPresenterCompl implements IWeatherPresenter, IOnFinished {

    private IWeatherView iWeatherView;
    private IWeather iWeather;

    public WeatherPresenterCompl(IWeatherView iWeatherView) {
        this.iWeatherView = iWeatherView;
    }

    @Override
    public void initializeModel() {
        iWeather = new WeatherModel(0, 0, null,
                null, true, true, Constant.ENABLE_LOCATION_INTERNET);
        iWeatherView.onInitializeModel((WeatherModel) iWeather);
    }

    @Override
    public void getLocationPermission() {
        iWeatherView.requesLocationPermission();
    }

    @Override
    public void getWeather(String lat, String lng) {
        iWeather.getWeather(this, lat, lng);
    }

    @Override
    public void setError(String message) {
        iWeatherView.onSetError(message);
    }

    @Override
    public void setProgressBar(int visible) {

    }

    @Override
    public void closeDialog() {
        iWeatherView.onCloseDialog();
    }

    @Override
    public void openDialog() {
        iWeatherView.onOpenDialog();
    }

    @Override
    public void setGpsDisabled(boolean status) {
        iWeatherView.onGpsDisabled(status);
    }

    @Override
    public void setInternetDisabled(boolean status) {
        iWeatherView.onInternetDisabled(status);
    }

    @Override
    public void isInternetConnected(boolean status) {
        if(status)
            iWeatherView.onInternetDisabled(false);
        else
            iWeatherView.onInternetDisabled(true);
    }

    @Override
    public void checkInternet() {
        iWeatherView.checkInternet();
    }

    @Override
    public void checkGpsInternet() {
        iWeatherView.onCheckGpsInternet();
    }

    @Override
    public void getLocation() {
        iWeatherView.onGetLocation();
    }

    @Override
    public void getLatLng(double lat, double lng) {
        iWeatherView.onSetLatLng(String.valueOf(lat), String.valueOf(lng));
    }

    @Override
    public void onFinished(ErrorInfo errorInfo) {
        if(errorInfo.getObject() != null)
            iWeatherView.onSetWeather((WeatherModel) errorInfo.getObject());
        else{
            initializeModel();
            iWeatherView.onSetError(errorInfo.getErrorMessage());
        }
    }
}
