package tournament.league.live.cricket.com.mvp.weather.view;

import tournament.league.live.cricket.com.mvp.weather.model.WeatherModel;

public interface IWeatherView {
    void onInitializeModel(WeatherModel weatherModel);
    void requesLocationPermission();
    void onSetWeather(WeatherModel weatherModel);
    void onSetError(String message);
//    Open phone settings for permission
    void onOpenSettings();
    void onCloseDialog();
    void onOpenDialog();
    void onGpsDisabled(boolean status);
    void onInternetDisabled(boolean status);
//    Use connectivity manager
    void checkInternet();
    void onCheckGpsInternet();
//    Use location manager
    void onGetLocation();
    void onSetLatLng(String lat, String lng);
}
