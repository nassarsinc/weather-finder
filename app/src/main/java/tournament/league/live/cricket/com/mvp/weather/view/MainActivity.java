package tournament.league.live.cricket.com.mvp.weather.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.Observable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import tournament.league.live.cricket.com.mvp.R;
import tournament.league.live.cricket.com.mvp.databinding.ActivityMainBinding;
import tournament.league.live.cricket.com.mvp.utility.Constant;
import tournament.league.live.cricket.com.mvp.utility.ErrorUtility;
import tournament.league.live.cricket.com.mvp.utility.PermissionUtility;
import tournament.league.live.cricket.com.mvp.weather.model.WeatherModel;
import tournament.league.live.cricket.com.mvp.weather.presenter.WeatherPresenterCompl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements IWeatherView {

    private ActivityMainBinding activityMainBinding;
    private WeatherPresenterCompl weatherPresenterCompl;
    private FragmentManager manager;

//    Observe changes to call API when gps and internet are enabled
    Observable.OnPropertyChangedCallback observable = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if(activityMainBinding.getWeatherModel().getCurrentlyModel() == null
                    && !activityMainBinding.getWeatherModel().isGpsDisabled()
                    && !activityMainBinding.getWeatherModel().isInternetDisabled())
                weatherPresenterCompl.getLocation();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Initialize binding
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        weatherPresenterCompl = new WeatherPresenterCompl(this);
        weatherPresenterCompl.initializeModel();
        activityMainBinding.setPresenter(weatherPresenterCompl);
        activityMainBinding.getWeatherModel().addOnPropertyChangedCallback(observable);

//        Check gps and internet
        weatherPresenterCompl.checkGpsInternet();
    }

    @Override
    public void onInitializeModel(WeatherModel weatherModel) {
        activityMainBinding.setWeatherModel(weatherModel);
    }

    @Override
    public void requesLocationPermission() {
        if(!PermissionUtility.checkHasPermission(this, Constant.FINE_LOCATION_PERMISSION))
            PermissionUtility.requestPermission(this, new String[]{Constant.FINE_LOCATION_PERMISSION},Constant.FINE_LOCATION_PERMISSION_CODE);
        else
            weatherPresenterCompl.setGpsDisabled(false);
    }

    @Override
    public void onSetWeather(WeatherModel weatherModel) {
        activityMainBinding.setWeatherModel(weatherModel);
    }

    @Override
    public void onSetError(String message) {
        activityMainBinding.getWeatherModel().setError(message);
    }

    @Override
    public void onOpenSettings() {
        Intent intent = new Intent().setAction(Constant.SETTING_ACTION_INTENT)
                .setData(Uri.fromParts("package", this.getPackageName(), null))
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(intent);
    }

    @Override
    public void onCloseDialog() {
        manager = getSupportFragmentManager();
        Fragment frag = manager.findFragmentByTag(Constant.SETTING_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
    }

    @Override
    public void onOpenDialog() {
        AlertDialogFragment alertDialogFragment = new AlertDialogFragment();
        alertDialogFragment.show(manager, Constant.SETTING_DIALOG_TAG);
    }

    @Override
    public void onGpsDisabled(boolean status) {
        activityMainBinding.getWeatherModel().setGpsDisabled(status);
    }

    @Override
    public void onInternetDisabled(boolean status) {
        activityMainBinding.getWeatherModel().setInternetDisabled(status);
    }

    @Override
    public void checkInternet() {
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            weatherPresenterCompl.isInternetConnected(isConnected);
        }
    }

    @Override
    public void onCheckGpsInternet() {
//        Check Gps Permission
        weatherPresenterCompl.getLocationPermission();
//        Check Internet
        weatherPresenterCompl.checkInternet();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onGetLocation() {
        LocationManager locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
        if (locationManager != null) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location != null)
                weatherPresenterCompl.getLatLng(location.getLatitude(), location.getLongitude());
            else {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null)
                    weatherPresenterCompl.getLatLng(location.getLatitude(), location.getLongitude());
                else {
                    weatherPresenterCompl.setInternetDisabled(true);
                    weatherPresenterCompl.setError(ErrorUtility.LOCATION_ISSUE.getMessage());
                }
            }
        }
    }

    @Override
    public void onSetLatLng(String lat, String lng) {
        weatherPresenterCompl.getWeather(lat, lng);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == Constant.FINE_LOCATION_PERMISSION_CODE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Permission given
                weatherPresenterCompl.setGpsDisabled(false);
            } else {
//                Permission denied
                weatherPresenterCompl.closeDialog();
                weatherPresenterCompl.openDialog();
            }
        }
    }
}
